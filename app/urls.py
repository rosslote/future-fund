from django.conf.urls import *
from django.conf import settings


urlpatterns = patterns('app.views',
    url(r'^$',
       'index',
        name="index"),
    url(r'^(?P<page>[-\w]+).html/', 'other_page'),
    url(r'^goal/(?P<goal_id>\d+)/$', 'goal'),
    url(r'^pricing/$', 'pricing'),
    url(r'^calculator/$', 'calculator'),
)