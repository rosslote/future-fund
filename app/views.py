from django.shortcuts import render

# Create your views here.

def index(request):
	return render(request,
		   'app/pages/index.html',
		   {})

def goal(request, goal_id):
	template = 'app/pages/savings-goal.html'
	return render(request,
		   template,
		   {'goal_id': goal_id})


def other_page(request, page):
	template = 'app/%s.html' % page
	return render(request,
		   template,
		   {})

def pricing(request):
	template = 'app/pages/price-chart.html'
	return render(request,
		   template,
		   {})

def calculator(request):
	template = 'app/pages/calculator.html'
	return render(request,
		   template,
		   {})